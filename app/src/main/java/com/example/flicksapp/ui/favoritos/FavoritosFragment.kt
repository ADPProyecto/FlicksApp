package com.example.flicksapp.ui.favoritos

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.flicksapp.R
import com.example.flicksapp.activities.main.*
import com.example.flicksapp.activities.principalNav.PrincipalNav
import com.example.flicksapp.databinding.FragmentFavoritosBinding
import com.mysql.jdbc.Statement
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException


class FavoritosFragment : Fragment() {

    private var _binding: FragmentFavoritosBinding? = null
    private var conexion: Connection? = null
    private var stmt: Statement? = null
    private var resultset: ResultSet? = null
    private var main:MainActivity ?= null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val favoritosViewModel = ViewModelProvider(this)[FavoritosViewModel::class.java]
        main = MainActivity()
        _binding = FragmentFavoritosBinding.inflate(inflater, container, false)
        val root: View = binding.root

        favoritosViewModel.text.observe(viewLifecycleOwner) {
            val main = (activity as PrincipalNav)
            val frag1 = TendenciasSubfragment()

            main.supportFragmentManager.beginTransaction().apply {
                add(R.id.fragmentTendencias0, frag1)
                commit()
            }
        }
        anadirArray()
        return root
    }

    private fun anadirArray(){

        textoPeliculasFav = ArrayList()
        textoMusicaFav = ArrayList()
        idCataPeliFav = ArrayList()
        idCataMusiFav = ArrayList()
        imagenesPeliculasFav = ArrayList()
        imagenesMusicaFav = ArrayList()

        try {
            val sqlVideo = "select IdCatalogo, Nombre, Imagen from Catalogo where Tipo='Video';"
            val sqlMusica = "select IdCatalogo, Nombre, Imagen from Catalogo where Tipo='Musica';"
            val sqlMusicaFav = "select IdCatalogo, Nombre from Catalogo where Tipo='Musica';"
            val favorito = "select * from Favoritos_Usuario WHERE IdUser='$idU' AND Favorito=true"

            conexion = main?.conexionBBDD()


            //Peliculas Fav
            arrayFavs(favorito,"Video", textoPeliculasFav as ArrayList<String>,
                idCataPeliFav as ArrayList<Int>, imagenesPeliculasFav as ArrayList<Bitmap>
            )

            //Musica Fav
            arrayFavs(favorito,"Musica", textoMusicaFav as ArrayList<String>,
                idCataMusiFav as ArrayList<Int>, imagenesMusicaFav as ArrayList<Bitmap>
            )

        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }


    private fun arrayFavs(a:String,tipo:String,array:MutableList<String> = ArrayList(), arrayIdCata: MutableList<Int>,c:MutableList<Bitmap>){
        stmt = conexion?.createStatement() as Statement?
        resultset = stmt?.executeQuery(a)
        if (stmt!!.execute(a)) {
            resultset = stmt!!.resultSet
        }
        while (resultset!!.next()) {
            idCat = resultset!!.getInt("IdCatalogo")
            arrayIdCata.add(idCat!!)
        }

        if(tipo=="Video") {
            for (idC in arrayIdCata) {
                val bVideo = "select Nombre, Imagen from Catalogo where IdCatalogo='$idC' AND Tipo='Video';"
                resultset = stmt?.executeQuery(bVideo)
                if (stmt!!.execute(bVideo)) {
                    resultset = stmt!!.resultSet
                }
                while (resultset!!.next()) {
                    val nombre = resultset!!.getString("Nombre").toString()
                    val i = resultset!!.getBlob("Imagen")

                    val blobLength:Int = i.length().toInt()
                    val blobsAsBytes:ByteArray = i.getBytes(1,blobLength)
                    val btm: Bitmap = BitmapFactory.decodeByteArray(blobsAsBytes,0,blobsAsBytes.size)

                    array.add(nombre)
                    c.add(btm)
                }
            }
        }else if(tipo == "Musica"){
            for (idC in arrayIdCata) {
                val bMusica = "select Nombre,Imagen from Catalogo where IdCatalogo='$idC' AND Tipo='Musica';"
                resultset = stmt?.executeQuery(bMusica)
                if (stmt!!.execute(bMusica)) {
                    resultset = stmt!!.resultSet
                }
                while (resultset!!.next()) {
                    val nombre = resultset!!.getString("Nombre").toString()
                    val i = resultset!!.getBlob("Imagen")

                    val blobLength:Int = i.length().toInt()
                    val blobsAsBytes:ByteArray = i.getBytes(1,blobLength)
                    val btm: Bitmap = BitmapFactory.decodeByteArray(blobsAsBytes,0,blobsAsBytes.size)

                    array.add(nombre)
                    c.add(btm)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}