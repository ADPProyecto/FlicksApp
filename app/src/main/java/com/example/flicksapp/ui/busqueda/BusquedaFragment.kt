package com.example.flicksapp.ui.busqueda

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.flicksapp.R
import com.example.flicksapp.activities.principalNav.PrincipalNav
import com.example.flicksapp.databinding.FragmentBusquedaBinding

class BusquedaFragment : Fragment() {
    private var _binding: FragmentBusquedaBinding? = null

    private val binding get() = _binding!!

    companion object{
        fun newInstance(): BusquedaFragment {
            val fragmentBusqueda = BusquedaFragment()
            val args = Bundle()
            fragmentBusqueda.arguments = args

            return fragmentBusqueda
        }
    }

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View {
        val vista = inflater.inflate(R.layout.fragment_busqueda,container,false)

        val busquedaViewModel = ViewModelProvider(this)[BusquedaViewModel::class.java]
        _binding = FragmentBusquedaBinding.inflate(inflater, container, false)
        val root: View = binding.root

        busquedaViewModel.text.observe(viewLifecycleOwner) {
            val main = (activity as PrincipalNav)
            val frag1 = BusquedaSubfragment()

            main.supportFragmentManager.beginTransaction().apply {
                add(R.id.fragmentBusqueda0, frag1)
                commit()
            }
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}