package com.example.flicksapp.ui.cuenta

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.flicksapp.R
import com.example.flicksapp.activities.principalNav.PrincipalNav
import com.example.flicksapp.databinding.FragmentCuentaBinding


class Cuenta : Fragment() {

    private var _binding: FragmentCuentaBinding? = null
    private val binding get() = _binding!!

    companion object {
        fun newInstance(): Cuenta {
            val fragmentCuenta = Cuenta()
            val args = Bundle()
            fragmentCuenta.arguments = args

            return fragmentCuenta
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View {
        // Inflate the layout for this fragment
        val vista = inflater.inflate(R.layout.fragment_cuenta,container,false)
        val principalNav = activity as PrincipalNav
        val recView = vista.findViewById<RecyclerView>(R.id.recyclerViewCuenta)
        val cuentaViewModel = ViewModelProvider(this)[CuentaViewModel::class.java]
        _binding = FragmentCuentaBinding.inflate(inflater, container, false)
        val root: View = binding.root

        cuentaViewModel.text.observe(viewLifecycleOwner) {
            val main = (activity as PrincipalNav)
            val frag1 = CuentaSubfragment()

            main.supportFragmentManager.beginTransaction().apply {
                add(R.id.fragmentCuenta0, frag1)
                commit()
            }
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}