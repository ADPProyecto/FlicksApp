package com.example.flicksapp.ui.home

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.flicksapp.*
import com.example.flicksapp.activities.main.*
import com.example.flicksapp.databinding.FragmentHomeBinding
import com.example.flicksapp.activities.principalNav.PrincipalNav
import com.mysql.jdbc.Statement
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private var conexion: Connection? = null
    private var stmt: Statement? = null
    private var resultset: ResultSet? = null
    private var main:MainActivity ?= null

    companion object{
        fun newInstance():HomeFragment{
            val fragmentHome = HomeFragment()
            val args = Bundle()
            fragmentHome.arguments = args

            return fragmentHome
        }
    }

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View {

        val homeViewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        main = MainActivity()
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        homeViewModel.text.observe(viewLifecycleOwner) {
            val main = (activity as PrincipalNav)
            val frag1 = HomeSubfragment()

            main.supportFragmentManager.beginTransaction().apply {
                add(R.id.fragmentHome0, frag1)
                commit()
            }
        }
        anadirArray()
        return root
    }

    private fun anadirArray(){
        textoPeliculas = ArrayList()
        textoMusica = ArrayList()
        imagenesPeliculas = ArrayList()
        imagenesMusica = ArrayList()
        esFavoritoPeliculas = ArrayList()
        esFavoritoMusica = ArrayList()

        try {
            val sqlVideo = "select IdCatalogo, Nombre, Imagen from Catalogo where Tipo='Video';"
            val sqlMusica = "select IdCatalogo, Nombre, Imagen from Catalogo where Tipo='Musica';"

            conexion = main?.conexionBBDD()

            //Peliculas
            arrayFull(sqlVideo, textoPeliculas as ArrayList<String>, imagenesPeliculas as ArrayList<Bitmap>,
                esFavoritoPeliculas as ArrayList<Boolean>)
            //Musica
            arrayFull(sqlMusica, textoMusica as ArrayList<String>, imagenesMusica as ArrayList<Bitmap>, esFavoritoMusica as ArrayList<Boolean>)

        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    private fun arrayFull(a:String, b:MutableList<String> = ArrayList(), c:MutableList<Bitmap> = ArrayList(), favArray:MutableList<Boolean>){
        stmt = conexion?.createStatement() as Statement?
        resultset = stmt?.executeQuery(a)
        if (stmt!!.execute(a)) {
            resultset = stmt!!.resultSet
        }
        while (resultset!!.next()) {
            val n = resultset!!.getString("Nombre").toString()
            val i = resultset!!.getBlob("Imagen")
            val idCat = resultset!!.getString("IdCatalogo")
            val blobLength:Int = i.length().toInt()
            val blobsAsBytes:ByteArray = i.getBytes(1,blobLength)
            val btm: Bitmap = BitmapFactory.decodeByteArray(blobsAsBytes,0,blobsAsBytes.size)
            b.add(n)
            c.add(btm)

            val bVideo = "select IdCatalogo,Favorito from Favoritos_Usuario where IdCatalogo='$idCat' and IdUser='$idU';"
            val stmt2: Statement? = conexion?.createStatement() as Statement?
            val resultset2: ResultSet? = stmt2?.executeQuery(bVideo)
            if (resultset2!!.next()) {
                favArray.add(true)
            }else{
                favArray.add(false)
            }



        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}