package com.example.flicksapp.ui.busqueda

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.flicksapp.R
import com.example.flicksapp.activities.main.*
import com.example.flicksapp.activities.principalNav.PrincipalNav
import com.example.flicksapp.adaptadores.*
import com.mysql.jdbc.Statement
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.text.SimpleDateFormat

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BusquedaSubfragment.newInstance] factory method to
 * create an instance of this fragment.
 */

var radioGroup: RadioGroup? = null
var radioPeliculas:RadioButton ?= null
var radioMusica:RadioButton ?= null

class BusquedaSubfragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var layoutManagerBusqueda: RecyclerView.LayoutManager? = null
    private var adaptBusqueda: RecyclerView.Adapter<AdaptadorBusqueda.ViewHolder>? = null
    private var layoutManagerHistorial: RecyclerView.LayoutManager? = null
    private var adaptHistorial: RecyclerView.Adapter<AdaptadorHistorial.ViewHolder>? = null

    private var conexion: Connection? = null
    private var stmt: Statement? = null
    private var resultset: ResultSet? = null
    private var main: MainActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val vista = inflater.inflate(R.layout.fragment_busqueda_subfragment, container, false)
        val busq = vista.findViewById<SearchView>(R.id.searchView)
        main = MainActivity()
        val principalNav = activity as PrincipalNav
        val recViewBusqueda = vista.findViewById<RecyclerView>(R.id.recyclerViewBusqueda)
        val recViewHistorial = vista.findViewById<RecyclerView>(R.id.recyclerViewHistorial)
        val textViewHistorial = vista.findViewById<TextView>(R.id.textViewHistorial)
        val noHay = vista.findViewById<TextView>(R.id.textViewNingunResultado)
        val noHayHistorial = vista.findViewById<TextView>(R.id.textViewNingunHistorial)
        val checkBoxGroup: LinearLayout = vista.findViewById(R.id.checkBoxGroup)
        val checkBox1: CheckBox = vista.findViewById(R.id.checkBox1)
        val checkBox2: CheckBox = vista.findViewById(R.id.checkBox2)
        val checkBox3: CheckBox = vista.findViewById(R.id.checkBox3)
        val butonBuscar: Button = vista.findViewById(R.id.buttonBusqueda)

        //Adaptador Historial
        historial = ArrayList()
        historialReproducciones = ArrayList()
        historialFecha = ArrayList()
        historialGenero = ArrayList()
        idHistorial = ArrayList()
        imagenesHistorial = ArrayList()

        try {

        val sqlHistorial = "select * from Historial_Usuario where IdUser=$idU"

            conexion = main!!.conexionBBDD()
            stmt = conexion?.createStatement() as Statement?
            resultset = stmt?.executeQuery(sqlHistorial)
            if (stmt!!.execute(sqlHistorial)) {
                resultset = stmt!!.resultSet
            }
            while (resultset!!.next()) {
                val nReproduccion = resultset!!.getString("N_Reproducciones")
                val simpleDate = SimpleDateFormat("dd/MM/yyyy HH:mm")
                val s: java.sql.Timestamp? = resultset!!.getTimestamp("FechaHistorial")
                val fechaReproduccion = simpleDate.format(s)
                idCat = resultset!!.getInt("IdCatalogo")
                idHistorial!!.add(idCat!!)
                historialFecha!!.add("Ultima reproducción: $fechaReproduccion")
                if(nReproduccion == 1.toString()){
                historialReproducciones!!.add("Reproducido $nReproduccion vez")
                }else {
                    historialReproducciones!!.add("Reproducido $nReproduccion veces")
                }
            }

            for (idC in idHistorial!!) {
                    val getter = "select Nombre, Imagen, Genero from Catalogo where IdCatalogo='$idC';"
                    resultset = stmt?.executeQuery(getter)
                    if (stmt!!.execute(getter)) {
                        resultset = stmt!!.resultSet
                    }
                    while (resultset!!.next()) {
                        val nombre = resultset!!.getString("Nombre").toString()
                        val i = resultset!!.getBlob("Imagen")
                        val g = resultset!!.getString("Genero")

                        val blobLength:Int = i.length().toInt()
                        val blobsAsBytes:ByteArray = i.getBytes(1,blobLength)
                        val btm: Bitmap = BitmapFactory.decodeByteArray(blobsAsBytes,0,blobsAsBytes.size)

                        historial!!.add(nombre)
                        imagenesHistorial!!.add(btm)
                        historialGenero!!.add("Genero: $g")
                    }
                }
        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }

        if (idHistorial!!.isEmpty()){
            noHayHistorial.visibility = View.VISIBLE
        }else{
            noHayHistorial.visibility = View.GONE
            recViewHistorial.visibility = View.VISIBLE
        }


        layoutManagerHistorial = LinearLayoutManager(principalNav)
        recViewHistorial.layoutManager = layoutManagerHistorial

        adaptHistorial = AdaptadorHistorial()
        recViewHistorial.adapter = adaptHistorial


        radioGroup = vista.findViewById(R.id.radioGroup)
        radioPeliculas = vista.findViewById(R.id.radioButtonPeliculas)
        radioMusica = vista.findViewById(R.id.radioButtonMusica)
        var sql:String ?= null

        radioMusica!!.setOnClickListener {
            checkBox1.text = "Musica electrónica"
            checkBox2.text = "Dance"
            checkBox3.text = "Trap"
            checkBox1.isChecked = false
            checkBox2.isChecked = false
            checkBox3.isChecked = false
            recViewBusqueda.visibility = View.GONE
            recViewHistorial.visibility = View.GONE
            textViewHistorial.visibility = View.GONE
            noHay.visibility = View.GONE
            checkBoxGroup.visibility = View.VISIBLE
        }

        radioPeliculas!!.setOnClickListener {
            checkBox1.text = "Comedia"
            checkBox2.text = "Terror"
            checkBox3.text = "Romance"
            checkBox1.isChecked = false
            checkBox2.isChecked = false
            checkBox3.isChecked = false
            recViewBusqueda.visibility = View.GONE
            recViewHistorial.visibility = View.GONE
            textViewHistorial.visibility = View.GONE
            noHay.visibility = View.GONE
            checkBoxGroup.visibility = View.VISIBLE
        }

        butonBuscar.setOnClickListener {
            busquedaPeliculas = ArrayList()
            busquedaMusica = ArrayList()
            busquedaGeneroPeliculas = ArrayList()
            busquedaGeneroMusica = ArrayList()
            busquedaAnioPeliculas = ArrayList()
            busquedaAnioMusica = ArrayList()

            if (checkBox1.isChecked) {
                sql =
                    "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Genero='${checkBox1.text}';"
                if(checkBox1.isChecked && checkBox2.isChecked){
                    sql =
                        "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Genero='${checkBox1.text}' or Genero='${checkBox2.text}';"
                }
                    if (checkBox1.isChecked && checkBox3.isChecked) {
                        sql =
                            "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Genero='${checkBox1.text}' or Genero='${checkBox3.text}';"
                    }
                        if(checkBox1.isChecked && checkBox2.isChecked && checkBox3.isChecked){
                            sql =
                                "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Genero='${checkBox1.text}' or Genero='${checkBox2.text}' or Genero='${checkBox3.text}';"
                        }
            }

            if (checkBox2.isChecked) {
                sql =
                    "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Genero='${checkBox2.text}';"
                if(checkBox2.isChecked && checkBox1.isChecked){
                    sql =
                        "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Genero='${checkBox2.text}' or Genero='${checkBox1.text}';"
                }
                if (checkBox2.isChecked && checkBox3.isChecked) {
                    sql =
                         "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Genero='${checkBox2.text}' or Genero='${checkBox3.text}';"
                    }
                if(checkBox2.isChecked && checkBox1.isChecked && checkBox3.isChecked){
                            sql =
                                "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Genero='${checkBox2.text}' or Genero='${checkBox3.text}' or Genero='${checkBox1.text}';"
                    }
                }

            if (checkBox3.isChecked) {
                sql =
                    "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Genero='${checkBox3.text}';"
                if (checkBox3.isChecked && checkBox1.isChecked) {
                    sql =
                        "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Genero='${checkBox3.text}' or Genero='${checkBox1.text}';"
                }
                if (checkBox3.isChecked && checkBox2.isChecked) {
                    sql =
                        "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Genero='${checkBox3.text}' or Genero='${checkBox2.text}';"
                }
                if (checkBox3.isChecked && checkBox1.isChecked && checkBox2.isChecked) {
                    sql =
                        "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Genero='${checkBox3.text}' or Genero='${checkBox1.text}' or Genero='${checkBox2.text}';"
                }
            }

            try {
                conexion = main!!.conexionBBDD()
                stmt = conexion?.createStatement() as Statement?
                resultset = stmt?.executeQuery(sql)
                if (stmt!!.execute(sql)) {
                    resultset = stmt!!.resultSet
                }
                while (resultset!!.next()) {
                    val n = resultset!!.getString("Nombre").toString()
                    val g = resultset!!.getString("Genero").toString()
                    val a = resultset!!.getInt("FechaSalida").toString()
                    if (radioPeliculas!!.isChecked) {
                        busquedaPeliculas!!.add(n)
                        busquedaGeneroPeliculas!!.add("Genero: $g")
                        busquedaAnioPeliculas!!.add("Año: $a")
                    } else {
                        if (radioMusica!!.isChecked) {
                            busquedaMusica!!.add(n)
                            busquedaGeneroMusica!!.add("Genero: $g")
                            busquedaAnioMusica!!.add("Año: $a")
                        }
                    }

                }
            } catch (ex: SQLException) {
                ex.printStackTrace()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            println("Enter buscado")
            recViewHistorial.visibility = View.GONE
            textViewHistorial.visibility = View.GONE
            recViewBusqueda.visibility = View.VISIBLE
            checkBoxGroup.visibility = View.GONE
            noHay.visibility = View.GONE

            layoutManagerBusqueda = LinearLayoutManager(principalNav)
            recViewBusqueda.layoutManager = layoutManagerBusqueda

            adaptBusqueda = AdaptadorBusqueda()
            recViewBusqueda.adapter = adaptBusqueda
        }

        busq.setOnQueryTextListener(object :SearchView.OnQueryTextListener{
            override fun onQueryTextChange(newText: String): Boolean {
                if(newText == ""){
                    busquedaPeliculas = ArrayList()
                    busquedaMusica = ArrayList()
                    busquedaGeneroPeliculas = ArrayList()
                    busquedaGeneroMusica = ArrayList()
                    busquedaAnioPeliculas = ArrayList()
                    busquedaAnioMusica = ArrayList()

                    layoutManagerBusqueda = LinearLayoutManager(principalNav)
                    recViewBusqueda.layoutManager = layoutManagerBusqueda

                    adaptBusqueda = AdaptadorBusqueda()
                    recViewBusqueda.adapter = adaptBusqueda
                }
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                if(!radioMusica!!.isChecked && !radioPeliculas!!.isChecked){
                    Toast.makeText(context,"Selecciona una categoria para buscar",Toast.LENGTH_SHORT).show()
                }

                    busquedaPeliculas = ArrayList()
                    busquedaMusica = ArrayList()
                    busquedaGeneroPeliculas = ArrayList()
                    busquedaGeneroMusica = ArrayList()
                    busquedaAnioPeliculas = ArrayList()
                    busquedaAnioMusica = ArrayList()

                    if (radioPeliculas!!.isChecked) {
                        sql =
                            "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Tipo='Video' AND (Nombre LIKE '$query%' or Genero LIKE '$query%' or FechaSalida LIKE '$query%');"
                    } else {
                        if (radioMusica!!.isChecked) {
                            sql =
                                "SELECT IdCatalogo,Nombre,Genero,FechaSalida FROM Catalogo WHERE Tipo='Musica' AND (Nombre LIKE '$query%' or Genero LIKE '$query%' or FechaSalida LIKE '$query%');"
                        }
                    }

                    try {
                        val hola = MainActivity()
                        conexion = hola.conexionBBDD()
                        stmt = conexion?.createStatement() as Statement?
                        resultset = stmt?.executeQuery(sql)
                        if (stmt!!.execute(sql)) {
                            resultset = stmt!!.resultSet
                        }
                        while (resultset!!.next()) {
                            val n = resultset!!.getString("Nombre").toString()
                            val g = resultset!!.getString("Genero").toString()
                            val a = resultset!!.getInt("FechaSalida").toString()
                            if (radioPeliculas!!.isChecked) {
                                busquedaPeliculas!!.add(n)
                                busquedaGeneroPeliculas!!.add("Genero: $g")
                                busquedaAnioPeliculas!!.add("Año: $a")
                            } else {
                                if (radioMusica!!.isChecked) {
                                    busquedaMusica!!.add(n)
                                    busquedaGeneroMusica!!.add("Genero: $g")
                                    busquedaAnioMusica!!.add("Año: $a")
                                }
                            }

                        }
                    } catch (ex: SQLException) {
                        ex.printStackTrace()
                    } catch (ex: Exception) {
                        ex.printStackTrace()
                    }

                    println("Enter buscado")
                    recViewBusqueda.visibility = View.VISIBLE
                    recViewHistorial.visibility = View.GONE
                    textViewHistorial.visibility = View.GONE
                    checkBoxGroup.visibility = View.GONE
                    noHay.visibility = View.GONE
                    layoutManagerBusqueda = LinearLayoutManager(principalNav)
                    recViewBusqueda.layoutManager = layoutManagerBusqueda

                    adaptBusqueda = AdaptadorBusqueda()
                    recViewBusqueda.adapter = adaptBusqueda

                    if((busquedaPeliculas.isNullOrEmpty() && busquedaMusica.isNullOrEmpty()) && (radioPeliculas!!.isChecked || radioMusica!!.isChecked)) {
                        recViewBusqueda.visibility = View.GONE
                        recViewHistorial.visibility = View.GONE
                        textViewHistorial.visibility = View.GONE
                        noHay.visibility = View.VISIBLE
                    }

                return false
            }
        })
        return vista
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BusquedaSubfragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            BusquedaSubfragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}