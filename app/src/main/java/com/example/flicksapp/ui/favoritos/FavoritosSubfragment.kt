package com.example.flicksapp.ui.favoritos

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.flicksapp.R
import com.example.flicksapp.activities.main.textoMusicaFav
import com.example.flicksapp.activities.main.textoPeliculasFav
import com.example.flicksapp.activities.principalNav.PrincipalNav
import com.example.flicksapp.adaptadores.AdaptadorMusicaFav
import com.example.flicksapp.adaptadores.AdaptadorPeliculasFav

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TendenciasSubfragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TendenciasSubfragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var layoutManagerPeliculas: RecyclerView.LayoutManager ?= null
    private var adaptPeliculas: RecyclerView.Adapter<AdaptadorPeliculasFav.ViewHolder> ?= null
    private var layoutManagerMusica: RecyclerView.LayoutManager ?= null
    private var adaptMusica: RecyclerView.Adapter<AdaptadorMusicaFav.ViewHolder> ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val vista = inflater.inflate(R.layout.fragment_favoritos_subfragment, container, false)
        val principalNav = activity as PrincipalNav
        val recViewPeliculas = vista.findViewById<RecyclerView>(R.id.recyclerViewPeliculasTendencias)
        val recViewMusica = vista.findViewById<RecyclerView>(R.id.recyclerViewMusicaTendencias)
        val noPelisFav = vista.findViewById<TextView>(R.id.textViewPeliculasFavoritos)
        val noMusicaFav = vista.findViewById<TextView>(R.id.textViewMusicaFavoritos)

        // Inflate the layout for this fragment
        layoutManagerPeliculas = LinearLayoutManager(principalNav,RecyclerView.HORIZONTAL,false)
        recViewPeliculas?.layoutManager = layoutManagerPeliculas

        adaptPeliculas = AdaptadorPeliculasFav()
        recViewPeliculas?.adapter = adaptPeliculas
        if(textoPeliculasFav.isNullOrEmpty()){
            recViewPeliculas.visibility = View.INVISIBLE
            noPelisFav.visibility = View.VISIBLE
        }


        layoutManagerMusica = LinearLayoutManager(principalNav,RecyclerView.HORIZONTAL,false)
        recViewMusica.layoutManager = layoutManagerMusica

        adaptMusica = AdaptadorMusicaFav()
        recViewMusica.adapter = adaptMusica
        if(textoMusicaFav.isNullOrEmpty()){
            recViewMusica.visibility = View.INVISIBLE
            noMusicaFav.visibility = View.VISIBLE
        }

        return vista
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TendenciasSubfragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TendenciasSubfragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}