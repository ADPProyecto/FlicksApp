package com.example.flicksapp.adaptadores

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.flicksapp.R
import com.example.flicksapp.activities.main.*

class AdaptadorHistorial: RecyclerView.Adapter<AdaptadorHistorial.ViewHolder>() {
    private var main:MainActivity ?= null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val a = LayoutInflater.from(parent.context).inflate(R.layout.fragment_historial_lista,parent,false)
        main = MainActivity()

        return ViewHolder(a)
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
            holder.itemTexto?.text = historial!![pos]
            holder.itemSubtexto?.text = historialGenero!![pos]
            holder.itemFechaHistorial?.text = historialFecha!![pos]
            holder.itemReproducciones?.text = historialReproducciones!![pos]
            holder.itemHistorialImagen?.setImageBitmap(imagenesHistorial!![pos])
    }

    override fun getItemCount(): Int {
        return historial!!.size
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var itemTexto: TextView ?= itemView.findViewById(R.id.textViewTextoHistorial)
        var itemSubtexto: TextView ?= itemView.findViewById(R.id.textViewSubtextoHistorial)
        var itemFechaHistorial: TextView ?= itemView.findViewById(R.id.textViewFechaHistorial)
        var itemReproducciones:TextView ?= itemView.findViewById(R.id.textViewReproducciones)
        var itemHistorialImagen:ImageView ?= itemView.findViewById(R.id.imagenHistorial)
    }
}