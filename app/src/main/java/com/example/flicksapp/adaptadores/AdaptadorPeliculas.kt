package com.example.flicksapp.adaptadores

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.flicksapp.R
import com.example.flicksapp.activities.main.*
import com.example.flicksapp.activities.reproductores.ReproductorPeliculas
import com.mysql.jdbc.Statement
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException

var idVideo:String ?= null

class AdaptadorPeliculas:RecyclerView.Adapter<AdaptadorPeliculas.ViewHolder>() {
    private var contextItemView: Context?= null
    private var intent:Intent ?= null

    private var conexion: Connection? = null
    private var stmt: Statement? = null
    private var resultset: ResultSet? = null
    private var nombre:String ?= null
    private var idCatalogo: Int? = null
    private var sql:String ?= null
    private var main:MainActivity?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val a = LayoutInflater.from(parent.context).inflate(R.layout.fragment_peliculas_lista,parent,false)
        main = MainActivity()

        return ViewHolder(a)
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.itemTexto.text = textoPeliculas!![pos]
        holder.itemImage.setImageBitmap(imagenesPeliculas!![pos])

        if(!esFavoritoPeliculas!![pos]){
            holder.favPeliculas.setImageResource(R.drawable.ic_fav_border)
        }else{
            if(esFavoritoPeliculas!![pos]){
                holder.favPeliculas.setImageResource(R.drawable.ic_fav)
            }
        }
    }

    override fun getItemCount(): Int {
        return textoPeliculas!!.size
    }

    inner class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        var itemImage:ImageView = itemView.findViewById(R.id.imageViewPeliculasLista)
        var itemTexto:TextView = itemView.findViewById(R.id.textoViewPeliculasLista)
        private var playPeliculas:ImageView = itemView.findViewById(R.id.imageViewPlayPeliculas)
        var favPeliculas:ImageView = itemView.findViewById(R.id.imageViewFavoritosPeliculas)

        init{
            //checkFav(itemView)
            playPeliculas.setOnClickListener{
                println("YESSSS")
                println(bindingAdapterPosition+1)
                when(bindingAdapterPosition+1){
                    1,2,3,4,5,6,7 -> {
                        conexionBaseDatos(bindingAdapterPosition+1)
                        openReproductor(itemView)
                        historial(bindingAdapterPosition+1)
                    }
                }
            }
            favPeliculas.setOnClickListener {
                when(bindingAdapterPosition+1){
                    1,2,3,4,5,6,7 -> {
                        setFav(bindingAdapterPosition+1,itemView.context,favPeliculas)
                    }
                }
            }
        }
    }

    private fun openReproductor(vista:View){
            contextItemView = vista.context
            intent = Intent(contextItemView, ReproductorPeliculas::class.java)
            contextItemView!!.startActivity(intent)
    }

    private fun setFav(num: Int,i:Context,image:ImageView) {


        try {
            conexion = main!!.conexionBBDD()
            nombre = textoPeliculas!![num - 1]

            val sqlIdCatalogo = "select IdCatalogo from Catalogo where Nombre='$nombre'"

            stmt = conexion?.createStatement() as Statement?
            resultset = stmt?.executeQuery(sqlIdCatalogo)
            if (stmt!!.execute(sqlIdCatalogo)) {
                resultset = stmt!!.resultSet
            }
            if (resultset!!.next()) {
                idCatalogo = resultset!!.getInt("IdCatalogo")
                val sqlCheck = "select * from Favoritos_Usuario where IdUser='$idU' and IdCatalogo='$idCatalogo'"
                stmt = conexion?.createStatement() as Statement?
                resultset = stmt?.executeQuery(sqlCheck)
                if (stmt!!.execute(sqlCheck)) {
                    resultset = stmt!!.resultSet
                }
                if (resultset!!.next()) {
                    println("Esta peli ya esta en favorito")
                    val sqlDelete = "delete from Favoritos_Usuario where idUser='$idU' and IdCatalogo='$idCatalogo'"
                    stmt?.executeUpdate(sqlDelete)
                    image.setImageResource(R.drawable.ic_fav_border)
                    Toast.makeText(i,"¡Eliminado de favoritos!",Toast.LENGTH_SHORT).show()
                } else {
                    val sqlInsert = "insert into Favoritos_Usuario values ($idU, $idCatalogo, 1);"
                    stmt?.executeUpdate(sqlInsert)
                    image.setImageResource(R.drawable.ic_fav)
                    Toast.makeText(i,"¡Añadido a favoritos!",Toast.LENGTH_SHORT).show()
                }
            } else {
                println("No existe esa peli")
            }

        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }

    private fun historial(num: Int) {

        try {
            conexion = main!!.conexionBBDD()
            nombre = textoPeliculas!![num-1]

            sql = "select * from Historial_Usuario where IdUser='$idU' and IdCatalogo='$idCatalogo'"

            stmt = conexion?.createStatement() as Statement?
            resultset = stmt?.executeQuery(sql)
            if (stmt!!.execute(sql)) {
                resultset = stmt!!.resultSet
            }
            if (resultset!!.next()) {
                println("Hay datos, pues hago un update")
                val sqlUpdate = "update Historial_Usuario set FechaHistorial = (now()+interval 2 hour), N_Reproducciones=N_Reproducciones+1 where " +
                                "IdUser='$idU' and IdCatalogo='$idCatalogo'"
                stmt?.executeUpdate(sqlUpdate)
            } else {
                println("No hay datos, pues hago un insert")
                val sqlInsert = "insert into Historial_Usuario values ($idU, $idCatalogo, 1, (now()+interval 2 hour), 1);"
                stmt?.executeUpdate(sqlInsert)

            }
        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun conexionBaseDatos(num:Int) {
            try {
                conexion = main!!.conexionBBDD()
                nombre = textoPeliculas!![num - 1]
                sql =
                    "select IdCatalogo, Nombre, Ruta from Catalogo where Nombre=binary'$nombre' and Tipo='Video';"
                stmt = conexion?.createStatement() as Statement?
                resultset = stmt?.executeQuery(sql)
                if (stmt!!.execute(sql)) {
                    resultset = stmt!!.resultSet
                }
                if (resultset!!.next()) {
                    val ruta = resultset!!.getString("Ruta")
                    if (resultset!!.row == 0) {
                        println("NOPE")
                    } else {
                        //if ()
                        idCatalogo = resultset!!.getInt("IdCatalogo")
                        idVideo = ruta
                    }
                } else {
                    println("NOPE")
                }
            } catch (ex: SQLException) {
                ex.printStackTrace()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
    }






















    /*
    private fun conexBDNotFav(num: Int) {
        try {
            conexion = main!!.conexionBBDD()
            nombre = textoPeliculas!![num - 1].toString()

            val sqlIdUser = "select U.IdUser, C.IdCatalogo from Usuarios as U, Catalogo as C where (U.Username='$u' or U.Email='$m') and C.Nombre='$nombre';"

            stmt = conexion?.createStatement() as Statement?
            resultset = stmt?.executeQuery(sqlIdUser)
            if (stmt!!.execute(sqlIdUser)) {
                resultset = stmt!!.resultSet
            }
            if (resultset!!.next()) {
                idUser = resultset!!.getInt("IdUser")
                idCatalogo = resultset!!.getInt("IdCatalogo")
                if (resultset!!.row == 0) {
                    println("No existe peli o user")
                } else {
                    val sqlIdCataUser = "select UC.IdCatalogo, UC.IdUser, U.username, C.Nombre " +
                            "from Favoritos_Usuario as UC inner join Usuarios as U on UC.idUser = U.IdUser" +
                            " inner join Catalogo as C on UC.IdCatalogo = C.IdCatalogo " +
                            "where UC.Favorito = 1 and C.Nombre = '$nombre' and UC.IdUser=$idUser;"
                    resultset = stmt?.executeQuery(sqlIdCataUser)
                    if (stmt!!.execute(sqlIdCataUser)) {
                        resultset = stmt!!.resultSet
                    }
                    if (resultset!!.next()) {
                        if (resultset!!.row == 0) {
                            // insert
                            println("Hay que hacer insert")
                        } else {
                            //update
                            println("hay que hacer update")
                            println("No hay que hacer nada porque ya esta favorito")
                            val sqlUpdate = "update Favoritos_Usuario set Favorito=false where IdUser='$idUser' and IdCatalogo='$idCatalogo'"
                            stmt = conexion?.createStatement() as Statement?
                            stmt?.executeUpdate(sqlUpdate)
                        }
                    } else {
                        println("NOP")
                        //val sqlInsert = "insert into Favoritos_Usuario values ('$idUser', '$idCatalogo', true)"
                        //stmt?.executeUpdate(sqlInsert)
                    }
                }
            }

        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
    private fun conexBDFav(num:Int) {
        try {
            conexion = main!!.conexionBBDD()
            nombre = textoPeliculas!![num - 1].toString()

            val sqlIdUser = "select U.IdUser, C.IdCatalogo from Usuarios as U, Catalogo as C where (U.Username='$u' or U.Email='$m') and C.Nombre='$nombre';"

            stmt = conexion?.createStatement() as Statement?
            resultset = stmt?.executeQuery(sqlIdUser)
            if (stmt!!.execute(sqlIdUser)) {
                resultset = stmt!!.resultSet
            }
            if (resultset!!.next()) {
                idUser = resultset!!.getInt("IdUser")
                idCatalogo = resultset!!.getInt("IdCatalogo")
                if (resultset!!.row == 0) {
                    println("No existe peli o user")
                } else {
                    val sqlIdCataUser = "select UC.IdCatalogo, UC.IdUser, U.username, C.Nombre " +
                            "from Favoritos_Usuario as UC inner join Usuarios as U on UC.idUser = U.IdUser" +
                            " inner join Catalogo as C on UC.IdCatalogo = C.IdCatalogo " +
                            "where UC.Favorito = 1 and C.Nombre = '$nombre' and UC.IdUser=$idUser;"
                    resultset = stmt?.executeQuery(sqlIdCataUser)
                    if (stmt!!.execute(sqlIdCataUser)) {
                        resultset = stmt!!.resultSet
                    }
                    if (resultset!!.next()) {
                        if (resultset!!.row == 0) {
                            // insert
                            println("Hay que hacer insert")
                        } else {
                            //update
                            println("hay que hacer update")
                            println("No hay que hacer nada porque ya esta favorito")
                            val sqlUpdate = "update Favoritos_Usuario set "
                        }
                    } else {
                        println("Otro insert")
                        val sqlInsert = "insert into Favoritos_Usuario values ('$idUser', '$idCatalogo', true)"
                        stmt?.executeUpdate(sqlInsert)
                    }
                }
            }

        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }*/
}