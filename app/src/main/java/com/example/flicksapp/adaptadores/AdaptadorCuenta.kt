package com.example.flicksapp.adaptadores

import android.content.Context
import android.content.Intent
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import com.example.flicksapp.R
import com.example.flicksapp.activities.main.*
import com.example.flicksapp.activities.principalNav.PrincipalNav
import com.mysql.jdbc.Statement
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException

private var conexion: Connection? = null
private var stmt: Statement? = null
private var resultset: ResultSet? = null
private var resultset2: Int? = null
private var main:MainActivity?=null
private var userDelete:String ?= null
private var emailDelete:String ?= null

class AdaptadorCuenta: RecyclerView.Adapter<AdaptadorCuenta.ViewHolder>() {
        private var texto:Array<String> =
            if (permisos==1) {
                arrayOf("Nombre usuario", "Email usuario", "Permisos","Cerrar sesión","Borrar usuario")
            }else{
                arrayOf("Nombre usuario", "Email usuario", "Permisos","Cerrar sesión")
            }
        private var subtexto =
            if (permisos==1) {
                arrayOf("$u", "$m", "Administrador", "Pulsa aquí para cerrar sesión.","Pulsa aquí para borrar un usuario basico")
            }else{
                arrayOf("$u","$m","Basico","Pulsa aquí para cerrar sesión.")
            }

        private var imagenes = if (permisos==1) {
            intArrayOf(
                R.drawable.ic_user5,
                R.drawable.ic_user5,
                R.drawable.ic_user5,
                R.drawable.ic_logout,
                R.drawable.ic_user5
            )
        }else{
            intArrayOf(
                R.drawable.ic_user5,
                R.drawable.ic_user5,
                R.drawable.ic_user5,
                R.drawable.ic_logout
            )
        }

        private var activity:PrincipalNav ?= null
        private var contextItemView: Context?= null
        private var intent:Intent ?= null


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val a = LayoutInflater.from(parent.context).inflate(R.layout.fragment_cuenta_lista,parent,false)
            main = MainActivity()

            return ViewHolder(a)
        }

        override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
            holder.itemTexto.text = texto[pos]
            holder.itemSubtexto.text = subtexto[pos]
            holder.itemImage.setImageResource(imagenes[pos])
        }

        override fun getItemCount(): Int {
            return texto.size
        }

        inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
            var itemImage: ImageView = itemView.findViewById(R.id.imageViewListaCuenta)
            var itemTexto: TextView = itemView.findViewById(R.id.textViewTextoCuenta)
            var itemSubtexto: TextView = itemView.findViewById(R.id.textViewSubtextoCuenta)

            init{
                itemView.setOnClickListener{
                    println("YESSSS")
                    println(bindingAdapterPosition+1)
                    when(bindingAdapterPosition+1){
                        4 -> {
                            println("Cerrar sesion")
                            alertDialogCerrarSesion(itemView)
                        }
                        5->{
                            alertDialogBorrarUsuario(itemView)
                        }
                    }
                }
            }
        }

        private fun alertDialogCerrarSesion(itemView: View){
            val builder = AlertDialog.Builder(itemView.context)
            builder.setTitle("Cierre sesión")
            builder.setMessage("¿Quieres cerrar sesión?")
            builder.setNegativeButton("No") { view,_ ->
                view.dismiss()
            }
            builder.setPositiveButton("Si") { view,_ ->
                activity = itemView.context as PrincipalNav
                contextItemView = itemView.context
                intent = Intent(contextItemView,MainActivity::class.java)
                activity!!.finish()
                contextItemView!!.startActivity(intent)
            }
            builder.show()
        }

        private fun alertDialogBorrarUsuario(itemView: View){
            val userBox = EditText(itemView.context)
            userBox.hint = "Usuario/Email"
            userBox.inputType = InputType.TYPE_CLASS_TEXT

            val builder = AlertDialog.Builder(itemView.context)
            builder.setView(userBox)

            builder.setTitle("Borrar usuario")
            builder.setMessage("Introduce el usuario o email de la cuenta a borrar")
            builder.setNegativeButton("Cancelar") { view,_ ->
            view.dismiss()
            }
               builder.setPositiveButton("Borrar") { view,_ ->
               val userText = userBox.text.toString()
                   comprobarUsuario(userText,itemView)
            }
         builder.show()
        }

    private fun borrarUsuario(i:View) {
        val contextItemView2 = i.context
        try {
            val sql = "delete from Usuarios where (Username=binary'$userDelete' or Email=binary'$emailDelete');"
            stmt = conexion?.createStatement() as Statement?
            resultset2 = stmt?.executeUpdate(sql)
            Toast.makeText(contextItemView2, "Usuario borrado correctamente", Toast.LENGTH_SHORT).show()
        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun comprobarUsuario(a:String,itemView:View) {
        val contextItemView2 = itemView.context
        try {
            conexion = main!!.conexionBBDD()
            val sql = "select Username, Email from Usuarios where (Username=binary'$a' or Email=binary'$a') and IdPermisos=2;"
            stmt = conexion?.createStatement() as Statement?
            resultset = stmt?.executeQuery(sql)
            if (stmt!!.execute(sql)) {
                resultset = stmt!!.resultSet
            }
            if (resultset!!.next()) {
                val row = resultset!!.row
                userDelete = resultset!!.getString("Username")
                emailDelete = resultset!!.getString("Email")
                if (resultset!!.row == 0) {
                    Toast.makeText(contextItemView2, "No existe el usuario o es un administrador", Toast.LENGTH_SHORT).show()
                } else {
                    borrarUsuario(itemView)
                    Toast.makeText(contextItemView2, "Usuario borrado correctamente", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(contextItemView2, "No existe el usuario o es un administrador", Toast.LENGTH_SHORT).show()
            }
        } catch (ex: SQLException) {
            ex.printStackTrace()
            Toast.makeText(contextItemView2, "No existe el usuario o es un administrador", Toast.LENGTH_SHORT).show()
        } catch (ex: Exception) {
            ex.printStackTrace()
            Toast.makeText(contextItemView2, "No existe el usuario o es un administrador", Toast.LENGTH_SHORT).show()
        }
    }
}