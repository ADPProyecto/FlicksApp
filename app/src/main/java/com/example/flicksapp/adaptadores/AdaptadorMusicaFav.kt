package com.example.flicksapp.adaptadores

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.flicksapp.R
import com.example.flicksapp.activities.main.*
import com.example.flicksapp.activities.principalNav.PrincipalNav
import com.example.flicksapp.activities.reproductores.ReproductorMusica
import com.mysql.jdbc.Statement
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException

class AdaptadorMusicaFav: RecyclerView.Adapter<AdaptadorMusicaFav.ViewHolder>() {
    private var activity: PrincipalNav?= null
    private var contextItemView: Context?= null
    private var intent: Intent?= null

    private var conexion: Connection? = null
    var stmt: Statement? = null
    var resultset: ResultSet? = null
    private var nombre:String ?= null
    private var sql:String ?= null
    private var main:MainActivity ?= null



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val a = LayoutInflater.from(parent.context).inflate(R.layout.fragment_musica_lista,parent,false)
        main = MainActivity()
        return ViewHolder(a)
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        holder.itemTexto.text = textoMusicaFav!![pos]
        holder.itemImage.setImageBitmap(imagenesMusicaFav!![pos])
    }

    override fun getItemCount(): Int {
        return textoMusicaFav!!.size
    }

    inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){
        var itemImage: ImageView = itemView.findViewById(R.id.imageViewMusicaLista)
        var itemTexto: TextView = itemView.findViewById(R.id.textoViewMusicaLista)
        private var playMusica:ImageView = itemView.findViewById(R.id.imageViewPlayMusica)
        private var favMusica:ImageView = itemView.findViewById(R.id.imageViewFavoritosMusica)

        init{
            favMusica.setImageResource(R.drawable.ic_fav)
            favMusica.tag = R.drawable.ic_fav
            playMusica.setOnClickListener{
                println(bindingAdapterPosition+1)
                when(bindingAdapterPosition+1){
                    1,2,3,4,5,6,7 -> {
                        conexionBaseDatos(bindingAdapterPosition+1)
                        openReproductor(itemView)
                        historial(bindingAdapterPosition+1)
                    }
                }
            }
            favMusica.setOnClickListener {
                println(bindingAdapterPosition+1)
                when(bindingAdapterPosition+1){
                    1,2,3,4,5,6,7 -> {
                        if (favMusica.tag == R.drawable.ic_fav) {
                            borrarFavorito(bindingAdapterPosition+1)
                            favMusica.setImageResource(R.drawable.ic_fav_border)
                            favMusica.tag = R.drawable.ic_fav_border
                            Toast.makeText(itemView.context,"¡Borrado de favoritos!",Toast.LENGTH_SHORT).show()
                        } else if (favMusica.tag == R.drawable.ic_fav_border) {
                            anadirFavorito(bindingAdapterPosition+1)
                            favMusica.setImageResource(R.drawable.ic_fav)
                            favMusica.tag = R.drawable.ic_fav
                            Toast.makeText(itemView.context,"¡Añadido otra vez a favoritos!",Toast.LENGTH_SHORT).show()
                        } 
                    }
                    
                }
            }
        }
    }

    private fun openReproductor(vista:View){
        activity = vista.context as PrincipalNav
        contextItemView = vista.context
        intent = Intent(contextItemView, ReproductorMusica::class.java)
        contextItemView!!.startActivity(intent)
    }

    private fun anadirFavorito(num: Int) {

        try {
            conexion = main!!.conexionBBDD()
            nombre = textoMusicaFav!![num-1]

            val sqlIdCatalogo = "select IdCatalogo from Catalogo where Nombre='$nombre'"
            stmt = conexion?.createStatement() as Statement?
            resultset = stmt?.executeQuery(sqlIdCatalogo)
            if (stmt!!.execute(sqlIdCatalogo)) {
                resultset = stmt!!.resultSet
            }
            if (resultset!!.next()) {
                idCatalogo = resultset!!.getInt("IdCatalogo")

                val sqlInsert = "insert into Favoritos_Usuario values ($idU, $idCatalogo, 1);"
                stmt?.executeUpdate(sqlInsert)


            } else {
                println("No existe esa peli")
            }
        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun borrarFavorito(num: Int) {

        try {
            conexion = main!!.conexionBBDD()
            nombre = textoMusicaFav!![num-1]

            val sqlIdCatalogo = "select IdCatalogo from Catalogo where Nombre='$nombre'"
            stmt = conexion?.createStatement() as Statement?
            resultset = stmt?.executeQuery(sqlIdCatalogo)
            if (stmt!!.execute(sqlIdCatalogo)) {
                resultset = stmt!!.resultSet
            }
            if (resultset!!.next()) {
                idCatalogo = resultset!!.getInt("IdCatalogo")
                println("se procede a borrarla")
                val sqlDelete = "delete from Favoritos_Usuario where idUser='$idU' and IdCatalogo='$idCatalogo'"
                stmt = conexion?.createStatement() as Statement?
                var resultset2: Int? = stmt?.executeUpdate(sqlDelete)
                println("Borrada")

            } else {
                println("No existe esa peli")
            }
        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun historial(num: Int) {

        try {
            conexion = main!!.conexionBBDD()
            nombre = textoPeliculas!![num-1]

            sql = "select * from Historial_Usuario where IdUser='$idU' and IdCatalogo='$idCatalogo'"

            stmt = conexion?.createStatement() as Statement?
            resultset = stmt?.executeQuery(sql)
            if (stmt!!.execute(sql)) {
                resultset = stmt!!.resultSet
            }
            if (resultset!!.next()) {
                println("Hay datos, pues hago un update")
                val sqlUpdate = "update Historial_Usuario set FechaHistorial = (now()+interval 2 hour), N_Reproducciones=N_Reproducciones+1 where " +
                        "IdUser='$idU' and IdCatalogo='$idCatalogo'"
                stmt?.executeUpdate(sqlUpdate)
            } else {
                println("No hay datos, pues hago un insert")
                val sqlInsert = "insert into Historial_Usuario values ($idU, $idCatalogo, 1, (now()+interval 2 hour), 1);"
                stmt?.executeUpdate(sqlInsert)

            }
        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun conexionBaseDatos(num:Int){
        try {
            conexion = main!!.conexionBBDD()
            nombre = textoMusicaFav!![num-1]
            sql = "select IdCatalogo, Nombre, Ruta from Catalogo where Nombre=binary'$nombre' and Tipo='Musica';"
            stmt = conexion?.createStatement() as Statement?
            resultset = stmt?.executeQuery(sql)
            if (stmt!!.execute(sql)) {
                resultset = stmt!!.resultSet
            }
            if (resultset!!.next()) {
                val ruta = resultset!!.getString("Ruta")
                if (resultset!!.row == 0) {
                    //Toast.makeText(context, "No hay Pelis ", Toast.LENGTH_SHORT).show()
                    println("NOPE")
                } else {
                    //if ()
                    idMusica = ruta
                    idCatalogo = resultset!!.getInt("IdCatalogo")
                    //Toast.makeText(context, "Login correcto", Toast.LENGTH_SHORT).show()
                }
            } else {
                println("NOPE")
                //Toast.makeText(context, "No hay pelis", Toast.LENGTH_SHORT).show()
            }
        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }
}