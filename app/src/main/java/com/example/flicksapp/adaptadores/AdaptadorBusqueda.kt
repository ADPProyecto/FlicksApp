package com.example.flicksapp.adaptadores

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.flicksapp.R
import com.example.flicksapp.activities.main.*
import com.example.flicksapp.activities.reproductores.ReproductorPeliculas
import com.example.flicksapp.activities.reproductores.ReproductorMusica
import com.example.flicksapp.ui.busqueda.radioMusica
import com.example.flicksapp.ui.busqueda.radioPeliculas
import com.mysql.jdbc.Statement
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException

class AdaptadorBusqueda: RecyclerView.Adapter<AdaptadorBusqueda.ViewHolder>() {
    private var contextItemView: Context?= null
    private var intent:Intent ?= null
    private var conexion: Connection? = null
    private var stmt: Statement? = null
    private var resultset: ResultSet? = null
    private var main:MainActivity ?= null
    private var nombre:String ?= null
    private var idCatalogo: Int? = null
    private var sql:String ?= null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val a = LayoutInflater.from(parent.context).inflate(R.layout.fragment_busqueda_lista,parent,false)
        main = MainActivity()

        return ViewHolder(a)
    }

    override fun onBindViewHolder(holder: ViewHolder, pos: Int) {
        if (radioMusica!!.isChecked){
            holder.itemTexto?.text = busquedaMusica!![pos]
            holder.itemSubtexto?.text = busquedaGeneroMusica!![pos]
            holder.itemAnio?.text = busquedaAnioMusica!![pos]
        }else{
            if (radioPeliculas!!.isChecked){
                holder.itemTexto?.text = busquedaPeliculas!![pos]
                holder.itemSubtexto?.text = busquedaGeneroPeliculas!![pos]
                holder.itemAnio?.text = busquedaAnioPeliculas!![pos]
            }
        }
    }

    override fun getItemCount(): Int {
        return if (radioMusica!!.isChecked){
            busquedaMusica!!.size
        }else {
            busquedaPeliculas!!.size
        }
    }

    inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var itemTexto: TextView ?= itemView.findViewById(R.id.textViewTextoBusqueda)
        var itemSubtexto: TextView ?= itemView.findViewById(R.id.textViewSubtextoBusqueda)
        var itemAnio: TextView ?= itemView.findViewById(R.id.textViewAnioBusqueda)

        init{
            itemView.setOnClickListener{
                println("YESSSS")
                println(bindingAdapterPosition+1)
                when(bindingAdapterPosition+1){
                    1,2,3,4,5,6,7 -> {
                        conexionBaseDatos(bindingAdapterPosition+1)
                        if (radioMusica!!.isChecked){
                            openReproductorMusica(itemView)
                            historial(bindingAdapterPosition+1)
                        }else {
                            if(radioPeliculas!!.isChecked) {
                                openReproductorPeliculas(itemView)
                                historial(bindingAdapterPosition+1)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun openReproductorPeliculas(vista:View){
        contextItemView = vista.context
        intent = Intent(contextItemView!!, ReproductorPeliculas::class.java)
        contextItemView!!.startActivity(intent!!)
    }

    private fun openReproductorMusica(vista:View){
        contextItemView = vista.context
        intent = Intent(contextItemView, ReproductorMusica::class.java)
        contextItemView!!.startActivity(intent)
    }

    private fun conexionBaseDatos(num:Int){
        try {
            conexion = main!!.conexionBBDD()
            var sql:String ?= null
            var nombre: String? = null
            if (radioMusica!!.isChecked){
                nombre = busquedaMusica!![num-1]
                sql = "select Nombre, Ruta, IdCatalogo from Catalogo where Nombre=binary'$nombre' and Tipo='Musica';"
            }else {
                if(radioPeliculas!!.isChecked) {
                    nombre = busquedaPeliculas!![num-1]
                    sql =
                        "select Nombre, Ruta,IdCatalogo from Catalogo where Nombre=binary'$nombre' and Tipo='Video';"
                }
            }

            stmt = conexion?.createStatement() as Statement?
            resultset = stmt?.executeQuery(sql)
            if (stmt!!.execute(sql)) {
                resultset = stmt!!.resultSet
            }
            if (resultset!!.next()) {
                val ruta = resultset!!.getString("Ruta")
                val idCat = resultset!!.getInt("IdCatalogo")
                if (resultset!!.row == 0) {
                    println("NOPE")
                } else {
                    idCatalogo = idCat
                    if (radioMusica!!.isChecked){
                        idMusica = ruta
                    }else{
                        if (radioPeliculas!!.isChecked){
                            idVideo = ruta
                        }
                    }
                }
            } else {
                println("NOPE")
            }
        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun historial(num: Int) {

        try {
            conexion = main!!.conexionBBDD()
            nombre = textoPeliculas!![num-1]

            sql = "select * from Historial_Usuario where IdUser='$idU' and IdCatalogo='$idCatalogo'"

            stmt = conexion?.createStatement() as Statement?
            resultset = stmt?.executeQuery(sql)
            if (stmt!!.execute(sql)) {
                resultset = stmt!!.resultSet
            }
            if (resultset!!.next()) {
                println("Hay datos, pues hago un update")
                val sqlUpdate = "update Historial_Usuario set FechaHistorial = (now()+interval 2 hour), N_Reproducciones=N_Reproducciones+1 where " +
                        "IdUser='$idU' and IdCatalogo='$idCatalogo'"
                stmt?.executeUpdate(sqlUpdate)
            } else {
                println("No hay datos, pues hago un insert")
                val sqlInsert = "insert into Historial_Usuario values ($idU, $idCatalogo, 1, (now()+interval 2 hour), 1);"
                stmt?.executeUpdate(sqlInsert)

            }
        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

}