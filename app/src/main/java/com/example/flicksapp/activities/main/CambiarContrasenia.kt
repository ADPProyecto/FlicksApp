package com.example.flicksapp.activities.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.example.flicksapp.R
import com.google.android.material.textfield.TextInputEditText
import com.mysql.jdbc.Statement
import java.sql.*

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class CambiarContrasenia : Fragment() {
    private var param1: String? = null
    private var param2: String? = null
    private var conexion: Connection? = null
    private var stmt: Statement? = null
    private var contrasenia1: String?= null
    private var contrasenia2: String?= null
    private var loginFragment:Login ?= null
    private var mainActivity:MainActivity ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val vista = inflater.inflate(R.layout.fragment_cambiar_contrasenia, container, false)
        val boton:Button = vista.findViewById(R.id.buttonCambiar)
        loginFragment = Login()
        mainActivity = activity as MainActivity
        boton.setOnClickListener {
            contrasenia2 = mainActivity?.findViewById<TextInputEditText>(R.id.textInputEditTextCambiarContrasenia2)?.text!!.toString()
            println(contrasenia2.toString())
            contrasenia1 = mainActivity?.findViewById<TextInputEditText>(R.id.textInputEditTextCambiarContrasenia)?.text!!.toString()
            println(contrasenia1.toString())
            cambiarPassword(loginFragment!!,mainActivity!!)
        }

        return vista
    }

    private fun toLogin(reg:Login,main2: MainActivity){
        main2.supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment0,reg)
            addToBackStack(null)
            commit()
        }
    }

    private fun cambiarPassword(login:Login,main: MainActivity) {


        if (contrasenia1!! == contrasenia2){
            try {
                conexion =  main.conexionBBDD()

                val sql = "UPDATE Usuarios SET Password='${contrasenia2!!}' WHERE Email='$emailContrasenia' AND Telefono='$telefonoContrasenia';"
                stmt = conexion?.createStatement() as Statement?
                stmt?.executeUpdate(sql)
                Toast.makeText(context,"Contraseña cambiada correctamente",Toast.LENGTH_SHORT).show()
                toLogin(login,main)
            } catch (ex: SQLException) {
                ex.printStackTrace()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }else{
            println("$contrasenia1 no es igual a $contrasenia2")
            Toast.makeText(context,"No coinciden las contraseñas", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CambiarContrasenia().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}