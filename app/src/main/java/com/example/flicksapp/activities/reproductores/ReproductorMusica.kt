package com.example.flicksapp.activities.reproductores

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.flicksapp.databinding.ActivityReproductorMusicaBinding
import com.google.android.exoplayer2.*
import com.example.flicksapp.adaptadores.idMusica
import java.lang.NullPointerException


class ReproductorMusica : AppCompatActivity() {
    private var player:SimpleExoPlayer ?= null
    private var viewBinding:ActivityReproductorMusicaBinding ?= null


    private var playWhenReady = true
    private var currentWindow = 0
    private var playbackPosition = 0L


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityReproductorMusicaBinding.inflate(layoutInflater)
        setContentView(viewBinding?.root)
    }

    private fun initializePlayer() {
        try{
        player = SimpleExoPlayer.Builder(this)
            .build()
            .also { exoPlayer ->
                viewBinding?.playerView?.player = exoPlayer

                val mediaItem = MediaItem.fromUri(idMusica!!)
                println(exoPlayer.currentTracksInfo.toString())
                exoPlayer.setMediaItem(mediaItem)
                exoPlayer.playWhenReady = playWhenReady
                exoPlayer.seekTo(currentWindow, playbackPosition)
                exoPlayer.prepare()
            }
    }catch (ex:NullPointerException){
        Toast.makeText(applicationContext,"¡No se ha podido reproducir, inténtalo de nuevo!", Toast.LENGTH_SHORT).show()
    }
    }


    override fun onStart() {
        super.onStart()
        initializePlayer()
    }

    override fun onResume() {
        super.onResume()
        if (player == null) {
            initializePlayer()
        }
    }

    override fun onPause() {
        super.onPause()
        releasePlayer()
    }

    override fun onStop() {
        super.onStop()
        releasePlayer()
    }

    private fun releasePlayer() {
        player?.run {
            playbackPosition = this.currentPosition
            currentWindow = this.currentWindowIndex
            playWhenReady = this.playWhenReady
            release()
        }
        player = null
    }

}