package com.example.flicksapp.activities.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.flicksapp.R
import com.google.android.material.textfield.TextInputEditText
import com.mysql.jdbc.Statement
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException


class Registro : Fragment(R.layout.fragment_registro) {
    private var conexion: Connection? = null
    private var stmt: Statement? = null
    private var textoNombre:String ?= null
    private var textoApellido:String ?= null
    private var textoUsuario:String ?= null
    private var textoEmail:String ?= null
    private var textoPassword:String ?= null
    private var textoTelefono:String ?= null
    private var resultset: ResultSet? = null
    private var main:MainActivity ?= null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        main = (activity as MainActivity)
        val primerFragment = Login()
        val vista = inflater.inflate(R.layout.fragment_registro, container, false)
        val boton = vista.findViewById<Button>(R.id.buttonRegistrar)

        boton.setOnClickListener {
            registroUsuario(primerFragment,main!!)
        }

        return vista
    }

    private fun registroUsuario(login:Login, main: MainActivity) {
        textoNombre = main.findViewById<TextInputEditText>(R.id.textInputEditTextNombreReg).text.toString()
        textoApellido = main.findViewById<TextInputEditText>(R.id.textInputEditTextApellidosReg).text.toString()
        textoUsuario = main.findViewById<TextInputEditText>(R.id.textInputEditTextUserReg).text.toString()
        textoEmail = main.findViewById<TextInputEditText>(R.id.textInputEditEmailReg).text.toString()
        textoPassword = main.findViewById<TextInputEditText>(R.id.textInputEditContraseniaReg).text.toString()
        textoTelefono = main.findViewById<TextInputEditText>(R.id.textInputEditTelefonoReg).text.toString()

        val list: MutableList<TextInputEditText> = ArrayList()

        list.add(main.findViewById(R.id.textInputEditTextUserReg))
        list.add(main.findViewById(R.id.textInputEditEmailReg))
        list.add(main.findViewById(R.id.textInputEditContraseniaReg))

        for (listitem in list) {
            if (listitem.text!!.contains("'")) {
                Toast.makeText(context,"¡Comillas Simples NO!",Toast.LENGTH_SHORT).show()
                listitem.text!!.clear()
                break
            }
        }



        try {
            conexion = main.conexionBBDD()
            val sqlUsuarios = "select Username, Email from Usuarios where (Username=binary'$textoUsuario' or Email=binary'$textoEmail');"
            val sqlRegistro = "INSERT INTO Usuarios (IdPermisos,Username,Password,Nombre,Apellidos,Email,Telefono) VALUES (2,'$textoUsuario','$textoPassword','$textoNombre','$textoApellido','$textoEmail',$textoTelefono);"
            stmt = conexion?.createStatement() as Statement?
            resultset = stmt?.executeQuery(sqlUsuarios)

            if (stmt!!.execute(sqlUsuarios)) {
                resultset = stmt!!.resultSet
            }
            if (!resultset!!.next()) {
                val row = resultset!!.row
                if (row != 0) {
                    Toast.makeText(context, "¡Ya hay un usuario registrado con ese nombre de usuario o email!", Toast.LENGTH_SHORT).show()
                    println("NOPE")
                } else {
                    println("$textoUsuario,$textoPassword,$textoNombre,$textoApellido,$textoEmail,$textoTelefono")
                    if(textoUsuario.toString() != "" || textoEmail.toString() != "" || textoPassword.toString() != "" || textoTelefono.toString() != "") {
                        stmt = conexion?.createStatement() as Statement?
                        stmt?.executeUpdate(sqlRegistro)
                        Toast.makeText(context,"¡Usuario $textoUsuario creado correctamente!",Toast.LENGTH_SHORT).show()
                        toLogin(login,main)
                    }else{
                        Toast.makeText(context,"¡Completa los campos!",Toast.LENGTH_SHORT).show()
                    }
                }
            } else {
                Toast.makeText(context, "¡Ya hay un usuario registrado con ese nombre de usuario o email!", Toast.LENGTH_SHORT).show()
            }

        } catch (ex: SQLException) {
            ex.printStackTrace()
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    private fun toLogin(reg:Login,main2: MainActivity){
        main2.supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment0,reg)
            addToBackStack(null)
            commit()
        }
    }
}