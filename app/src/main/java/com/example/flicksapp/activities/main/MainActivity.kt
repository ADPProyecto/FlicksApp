package com.example.flicksapp.activities.main

import android.content.Context
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.os.StrictMode
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.flicksapp.R
import java.sql.DriverManager

var busquedaPeliculas: MutableList<String> ?= null
var busquedaMusica: MutableList<String> ?= null
var busquedaGeneroPeliculas: MutableList<String> ?= null
var busquedaGeneroMusica: MutableList<String> ?= null
var busquedaAnioPeliculas: MutableList<String> ?= null
var busquedaAnioMusica: MutableList<String> ?= null

var textoPeliculas: MutableList<String> ?= null
var textoMusica: MutableList<String> ?= null

var textoPeliculasFav: MutableList<String> ?= null
var textoMusicaFav: MutableList<String> ?= null
var esFavoritoPeliculas: MutableList<Boolean> ?= null
var esFavoritoMusica: MutableList<Boolean> ?= null
var idCataPeliFav: MutableList<Int> ?= null
var idCataMusiFav: MutableList<Int> ?= null

var imagenesPeliculas: MutableList<Bitmap> ?= null
var imagenesMusica: MutableList<Bitmap> ?= null
var imagenesPeliculasFav: MutableList<Bitmap> ?= null
var imagenesMusicaFav: MutableList<Bitmap> ?= null
var imagenesHistorial: MutableList<Bitmap> ?= null

var historial:MutableList<String> ?= null
var historialFecha:MutableList<String> ?= null
var historialGenero:MutableList<String> ?= null
var historialReproducciones:MutableList<String> ?= null
var idHistorial:MutableList<Int> ?= null



class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val firstFragment = Login()

            supportFragmentManager.beginTransaction().apply {
            add(R.id.fragment0,firstFragment)
            commit()
        }
    }

    fun conexionBBDD(): java.sql.Connection? {
        Class.forName("com.mysql.jdbc.Driver").newInstance()
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        return DriverManager.getConnection(
            "jdbc:mysql://flicksapp.csltt4ecvjbg.eu-west-3.rds.amazonaws.com:3306/FlickApp",
            "adminADP", "Adp1234.#"
        )
    }

    fun conexionInternet():Boolean{
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true
        val isNotConnected: Boolean = activeNetwork?.isConnectedOrConnecting == false
        return if(isConnected){
            isConnected
        }else{
            Toast.makeText(applicationContext,"NO HAY INTERNET",Toast.LENGTH_SHORT).show()
            isNotConnected
        }
    }
}