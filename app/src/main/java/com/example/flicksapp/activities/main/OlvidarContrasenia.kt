package com.example.flicksapp.activities.main

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.example.flicksapp.R
import com.google.android.material.textfield.TextInputEditText
import com.mysql.jdbc.Statement
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException

var emailContrasenia:String?= null
var telefonoContrasenia:String?=null

class OlvidarContrasenia : Fragment() {
    private var resultset: ResultSet? = null
    private var conexion: Connection? = null
    private var stmt: Statement? = null
    private var textoEmail: Editable?= null
    private var textoTelefono: Editable?= null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val vista = inflater.inflate(R.layout.fragment_olvidar_contrasenia, container, false)
        val botonOlvidar = vista.findViewById<Button>(R.id.buttonEnviar)
        val main = activity as MainActivity
        val cambiarContrasenia = CambiarContrasenia()

        botonOlvidar.setOnClickListener {
            comprobarUsuario(cambiarContrasenia,main)
        }

        return vista
    }

    private fun cambiarContrasenia(reg2:CambiarContrasenia,main3: MainActivity){
        main3.supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment0,reg2)
            addToBackStack(null)
            commit()
        }
    }


    private fun comprobarUsuario(cambiarPass:CambiarContrasenia,main: MainActivity) {
        val cm = main.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
        val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

        if (isConnected) {
            textoEmail = main.findViewById<TextInputEditText>(R.id.textInputEditTextOlvidarContraseniaEmail)?.text
            textoTelefono = main.findViewById<TextInputEditText>(R.id.textInputEditTextOlvidarContraseniaTelefono)?.text

            try {
                conexion = main.conexionBBDD()
                val sql = "SELECT Email,Telefono FROM FlickApp.Usuarios WHERE Email='${textoEmail.toString()}' AND Telefono='${textoTelefono.toString()}';"

                stmt = conexion?.createStatement() as Statement?
                resultset = stmt?.executeQuery(sql)
                if (stmt!!.execute(sql)) {
                    resultset = stmt!!.resultSet
                }
                if (resultset!!.next()) {
                    val row = resultset!!.row
                    if (resultset!!.row == 0) {
                        Toast.makeText(context, "¡No existen esos datos!", Toast.LENGTH_SHORT).show()
                        println("NOPE")
                    } else {
                        emailContrasenia = textoEmail.toString()
                        telefonoContrasenia = textoTelefono.toString()
                        cambiarContrasenia(cambiarPass,main)
                    }
                } else {
                    Toast.makeText(context, "¡No existen esos datos!", Toast.LENGTH_SHORT).show()
                }

            } catch (ex: SQLException) {
                ex.printStackTrace()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }else{
            Toast.makeText(context,"NO HAY INTERNET",Toast.LENGTH_SHORT).show()
        }
    }
}