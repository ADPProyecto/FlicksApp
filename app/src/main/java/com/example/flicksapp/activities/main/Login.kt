package com.example.flicksapp.activities.main

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.flicksapp.R
import com.example.flicksapp.R.*
import com.example.flicksapp.activities.principalNav.PrincipalNav
import com.google.android.material.textfield.TextInputEditText
import com.mysql.jdbc.Statement
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException

var idU:Int ?= null
var u:String ?= null
var m:String ?= null
var p:String ?= null
var permisos:Int ?= null
var idCat:Int? = null
var idCatalogo: Int? = null

class Login : Fragment() {

    private var conexion: Connection? = null
    private var stmt: Statement? = null
    private var resultset: ResultSet? = null
    private var main:MainActivity ?= null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View?
    {
        val vista = inflater.inflate(layout.fragment_login, container, false)
        val boton:Button = vista.findViewById(R.id.buttonLogin)
        val regClick:TextView = vista.findViewById(R.id.textViewRegistrarse)
        val olvClick:TextView = vista.findViewById(R.id.textViewOlvidarContrasenia)
        val secondFragment = Registro()
        val thirdFragment = OlvidarContrasenia()
        main = (activity as MainActivity)


            boton.setOnClickListener {
                if(main!!.conexionInternet()) {
               conexionBaseDatos(main!!)
                }
            }

            regClick.setOnClickListener {
                if(main!!.conexionInternet()) {
                    registrarse(secondFragment, main!!)
                }
            }

            olvClick.setOnClickListener {
                if(main!!.conexionInternet()) {
                    olvidarContrasenia(thirdFragment, main!!)
                }
        }

        return vista
    }

    private fun listenerToAnotherActivity(main: MainActivity){
            val intent = Intent(main, PrincipalNav::class.java)
            startActivity(intent)
            main.finish()
    }

    private fun registrarse(reg:Registro,main2: MainActivity){
        main2.supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment0,reg)
            addToBackStack(null)
            commit()
        }
    }

    private fun olvidarContrasenia(reg2:OlvidarContrasenia,main3: MainActivity){
        main3.supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment0,reg2)
            addToBackStack(null)
            commit()
        }
    }

    private fun conexionBaseDatos(main: MainActivity) {
        if (main.conexionInternet()) {
            var user = main.findViewById<TextInputEditText>(R.id.textInputEditTextUser).text.toString()
            var pass = main.findViewById<TextInputEditText>(R.id.textInputEditTextPass).text.toString()
            var mail = main.findViewById<TextInputEditText>(R.id.textInputEditTextUser).text.toString()

            user = user.replace("'", "")
            mail = mail.replace("'", "")
            pass = pass.replace("'", "")

            try {
                conexion = main.conexionBBDD()
                val sql = "select IdUser, IdPermisos, Username, Email, Password from Usuarios where (Username=binary'$user' or Email=binary'$mail') and Password=binary'$pass';"
                stmt = conexion?.createStatement() as Statement?
                resultset = stmt?.executeQuery(sql)
                if (stmt!!.execute(sql)) {
                    resultset = stmt!!.resultSet
                }
                if (resultset!!.next()) {
                    val row = resultset!!.row
                    idU = resultset!!.getInt("IdUser")
                    permisos = resultset!!.getInt("IdPermisos")
                    u = resultset!!.getString("Username")
                    m = resultset!!.getString("Email")
                    p = resultset!!.getString("Password")
                    if (resultset!!.row == 0) {
                        Toast.makeText(context, "Usuario o contraseña incorrecta", Toast.LENGTH_SHORT).show()
                        println("NOPE")
                    } else {
                        listenerToAnotherActivity(main)
                        Toast.makeText(context, "¡Bienvenido $user!", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(context, "Usuario o contraseña incorrecta", Toast.LENGTH_SHORT).show()
                }

            } catch (ex: SQLException) {
                ex.printStackTrace()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }
}