package com.example.flicksapp.activities.principalNav

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.flicksapp.R
import com.example.flicksapp.databinding.ActivityPrincipalNavBinding
import com.google.android.material.bottomnavigation.BottomNavigationView

class PrincipalNav : AppCompatActivity() {

    private lateinit var binding: ActivityPrincipalNavBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityPrincipalNavBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_principal_nav)

        navView.setupWithNavController(navController)
    }

    override fun onBackPressed() {
        moveTaskToBack(true)
    }
}