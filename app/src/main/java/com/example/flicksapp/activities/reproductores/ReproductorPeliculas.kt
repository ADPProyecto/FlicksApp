package com.example.flicksapp.activities.reproductores

import android.os.Bundle
import android.widget.Toast
import com.example.flicksapp.R
import com.example.flicksapp.adaptadores.idVideo
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView
import java.lang.NullPointerException


class ReproductorPeliculas : YouTubeBaseActivity(){

    private lateinit var youtubePlayerInit : YouTubePlayer.OnInitializedListener
    private var youtubePlayer:YouTubePlayerView ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reproductor)

        youtubePlayer = findViewById(R.id.player)
        try {
            youtubePlayerInit = object : YouTubePlayer.OnInitializedListener {
                override fun onInitializationSuccess(
                    p0: YouTubePlayer.Provider?,
                    p1: YouTubePlayer?,
                    p2: Boolean
                ) {
                    try {
                        p1?.setFullscreen(true)
                        p1?.cueVideo(idVideo!!)
                        p1?.play()
                    }catch (ex:NullPointerException){
                        Toast.makeText(applicationContext,"¡No se ha podido reproducir, inténtalo de nuevo!",Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onInitializationFailure(
                    p0: YouTubePlayer.Provider?,
                    p1: YouTubeInitializationResult?
                ) {

                }
            }
        }catch (ex:NullPointerException){
            Toast.makeText(applicationContext,"¡No se ha podido reproducir, inténtalo de nuevo!",Toast.LENGTH_SHORT).show()
        }
        youtubePlayer?.initialize(R.string.api_key.toString(),youtubePlayerInit)
    }


}