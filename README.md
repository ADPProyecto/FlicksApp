# FlicksApp

Aplicación de reproducción multimedia así como películas y música en streaming, todo en uno.

Descárgala en el siguiente enlace: \
https://adpproyecto.applivery.io/flicksapp

## Pre-requisitos 📋
Versión mínima Android 7.0.\
Resolución mínima del dispositivo 6".

### Instalación 🔧
Para poder instalar la aplicación es necesario tener habilitado en los ajustes del dispositivo la posibilidad de poder instalar aplicaciones de fuentes externas.

## Lenguaje 🛠️
Kotlin.

## Autores ✒️
Alejandro Torres Guerra. \
Daniel Navarro García. \
Pablo Calderón Rodríguez.
